function performReset() {
    document.getElementById("inputId").value = "";
    document.getElementById("inputCounty").value = "";
    document.getElementById("inputVillage").value = "";
    document.getElementById("inputArea").value = "";
    document.getElementById("inputParcel").value = "";
    document.getElementById("inputField").value = "";
    document.getElementById("inputSurface").value = "";
    document.getElementById("inputBuyer").value = "";
    filterTable(event, 1);
}


function filterTable(event, index) {
    var filter = event.target.value.toUpperCase();
    var rows = document.querySelector("#parcel_t tbody").rows;
    for (var i = 0; i < rows.length; i++) {
        var firstCol = rows[i].cells[0].textContent.toUpperCase();
        var secondCol = rows[i].cells[1].textContent.toUpperCase();
        var thirdCol = rows[i].cells[2].textContent.toUpperCase();
        var fourthCol = rows[i].cells[3].textContent.toUpperCase();
        var fifthCol = rows[i].cells[4].textContent.toUpperCase();
        var sixthCol = rows[i].cells[5].textContent.toUpperCase();
        var seventhCol = rows[i].cells[6].textContent.toUpperCase();
        var eighthCol = rows[i].cells[7].textContent.toUpperCase();

        if ((firstCol.indexOf(filter) > -1 && index == -1) ||
            (secondCol.indexOf(filter) > -1 && index == 0) ||
            (thirdCol.indexOf(filter) > -1 && index == 1) ||
            (fourthCol.indexOf(filter) > -1 && index == 2) ||
            (fifthCol.indexOf(filter) > -1 && index == 3) ||
            (sixthCol.indexOf(filter) > -1 && index == 4) ||
            (seventhCol.indexOf(filter) > -1 && index == 5) ||
            (eighthCol.indexOf(filter) > -1 && index == 6)) {
            rows[i].style.display = '';
        } else {
            rows[i].style.display = 'none';
        }
    }
}

function setsearch() {

    document.querySelectorAll('input').forEach(function (el, idx) {
        el.addEventListener('keyup', function (e) {
            filterTable(e, idx);
        }, false);
    });
}