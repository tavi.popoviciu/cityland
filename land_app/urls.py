from django.urls import path
from land_app import views

urlpatterns = [

    path('', views.ParcelListView.as_view(), name='parcel_list'),
    path('county_add', views.CountyCreateView.as_view(), name='county_add'),
    path('county_list', views.CountyListView.as_view(), name='county_list'),
    path('county_details/<int:pk>', views.CountyDetailView.as_view(), name='county_details'),
    path('county_update/<int:pk>', views.CountyUpdateView.as_view(), name='county_update'),
    path('county_delete/<int:pk>', views.CountyDeleteView.as_view(), name='county_delete'),

    path('village_add', views.VillageCreateView.as_view(), name='village_add'),
    path('village_list', views.VillageListView.as_view(), name='village_list'),
    path('village_details/<int:pk>', views.VillageDetailView.as_view(), name='village_details'),
    path('village_update/<int:pk>', views.VillageUpdateView.as_view(), name='village_update'),
    path('village_delete/<int:pk>', views.VillageDeleteView.as_view(), name='village_delete'),

    path('area_add', views.AreaCreateView.as_view(), name='area_add'),
    path('area_list', views.AreaListView.as_view(), name='area_list'),
    path('area_details/<int:pk>', views.AreaDetailView.as_view(), name='area_details'),
    path('area_update/<int:pk>', views.AreaUpdateView.as_view(), name='area_update'),
    path('area_delete/<int:pk>', views.AreaDeleteView.as_view(), name='area_delete'),

    path('owner_add', views.OwnerCreateView.as_view(), name='owner_add'),
    path('owner_list', views.OwnerListView.as_view(), name='owner_list'),
    path('owner_details/<int:pk>', views.OwnerDetailView.as_view(), name='owner_details'),
    path('owner_update/<int:pk>', views.OwnerUpdateView.as_view(), name='owner_update'),
    path('owner_delete/<int:pk>', views.OwnerDeleteView.as_view(), name='owner_delete'),

    path('parcel_add', views.ParcelCreateView.as_view(), name='parcel_add'),
    path('parcel_list', views.ParcelListView.as_view(), name='parcel_list'),
    path('parcel_details/<int:pk>', views.ParcelDetailView.as_view(), name='parcel_details'),
    path('parcel_update/<int:pk>', views.ParcelUpdateView.as_view(), name='parcel_update'),
    path('parcel_delete/<int:pk>', views.ParcelDeleteView.as_view(), name='parcel_delete'),

    path('contract_add', views.ContractCreateView.as_view(), name='contract_add'),
    path('contract_list', views.ContractListView.as_view(), name='contract_list'),
    path('contract_details/<int:pk>', views.ContractDetailView.as_view(), name='contract_details'),
    path('contract_update/<int:pk>', views.ContractUpdateView.as_view(), name='contract_update'),
    path('contract_delete/<int:pk>', views.ContractDeleteView.as_view(), name='contract_delete'),

    path('contract_parcel_list', views.ContractParcelListView.as_view(), name='contract_parcel_list'),
    path('contract_parcel_details/<int:pk>', views.ContractParcelDetailView.as_view(), name='contract_parcel_details'),
    path('contract_parcel_add/<int:pk>', views.ParcelAddView.as_view(), name='contract_parcel_add'),

    path('lease_add', views.LeaseCreateView.as_view(), name='lease_add'),
    path('lease_list', views.LeaseListView.as_view(), name='lease_list'),
    path('lease_details/<int:pk>', views.LeaseDetailView.as_view(), name='lease_details'),
    path('lease_update/<int:pk>', views.LeaseUpdateView.as_view(), name='lease_update'),
    path('lease_delete/<int:pk>', views.LeaseDeleteView.as_view(), name='lease_delete'),

    path('lease_parcel_list', views.LeaseParcelListView.as_view(), name='lease_parcel_list'),
    path('lease__parcel_details/<int:pk>', views.LeaseParcelDetailView.as_view(), name='lease_parcel_details'),

    path('parcel_select', views.contract_parcel_select, name='parcel_select'),

]
