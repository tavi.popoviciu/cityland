from django.db import models


# Create your models here.


class County(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Village(models.Model):
    name = models.CharField(max_length=30)
    county = models.ForeignKey(
        'County',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return self.name + "(" + self.county.name + ")"


class Area(models.Model):
    name = models.CharField(max_length=30)
    village = models.ForeignKey(
        'Village',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return self.name + "(" + self.village.name + "+" + self.village.county.name + ")"


class Owner(models.Model):
    status = models.CharField(max_length=30, default="PF")
    name = models.CharField(max_length=30)
    personal_no = models.CharField(max_length=30)
    series_id = models.CharField(max_length=30)
    number_id = models.CharField(max_length=30)
    adress = models.CharField(max_length=30)
    ro_reg_id = models.CharField(max_length=30)
    ro_reg_position = models.CharField(max_length=30)
    anexa = models.IntegerField()
    email = models.CharField(max_length=30)
    phone = models.CharField(max_length=13)

    def __str__(self):
        return self.name
    # if self.status=="1"    return "PF"
    # elif self.status=="0"  return "PJ"


class Parcel(models.Model):
    number = models.CharField(max_length=30)
    field_no = models.CharField(max_length=30)
    surface = models.DecimalField(max_digits=10, decimal_places=2)
    surface_measured = models.DecimalField(max_digits=10, decimal_places=2)
    cathegory = models.CharField(max_length=20, default=None)
    land_reg_no = models.CharField(max_length=30, default=None)
    land_reg_id = models.CharField(max_length=30, default=None)
    land_reg_owner = models.CharField(max_length=30, default=None)
    title_no = models.CharField(max_length=30, default=None)
    title_date = models.DateTimeField()
    title_owner = models.CharField(max_length=30, default=None)
    observation = models.CharField(max_length=50, default=None)
    gis_no = models.CharField(max_length=30)
    area = models.ForeignKey(
        'Area',
        on_delete=models.DO_NOTHING,
    )
    owner = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
    )


class Contract(models.Model):
    number = models.CharField(max_length=30)
    date = models.DateTimeField()
    type = models.IntegerField()
    observation = models.CharField(max_length=50)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    surface = models.DecimalField(max_digits=10, decimal_places=2)
    buyer = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
    )


class Lease(models.Model):
    number = models.CharField(max_length=30)
    date = models.DateTimeField()
    type = models.IntegerField()
    leas_holder = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
        related_name='leas_holder'
    )
    leaser = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
        related_name='leaser'
    )
    currencyType = models.IntegerField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    observation = models.CharField(max_length=50)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    surface = models.DecimalField(max_digits=10, decimal_places=2)
    closed = models.BooleanField()


class ContractParcel(models.Model):
    contract = models.ForeignKey(
        'Contract',
        on_delete=models.DO_NOTHING,
    )

    parcel = models.ForeignKey(
        'Parcel',
        on_delete=models.DO_NOTHING,
    )

    seller = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
    )


class LeaseParcel(models.Model):
    lease = models.ForeignKey(
        'Lease',
        on_delete=models.DO_NOTHING,
    )

    parcel = models.ForeignKey(
        'Parcel',
        on_delete=models.DO_NOTHING,
    )

    leas_holder = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
        related_name='%(class)s_leas_holder'
    )

    leaser = models.ForeignKey(
        'Owner',
        on_delete=models.DO_NOTHING,
        related_name='%(class)s_leaser')
#
# parcela 1/1/2 FirmID=0000 (Nea iLie) Id=1111
# parcela 1/1/2 FirmID=0001 (Sora iLie) Id=1112
# contract xxx BuyerId (Asi) Id=2222 DATA
# contractparcel ParcelId = 1111 ContractID = xxx SellerId = 0000
#
#
# contract zzz BuyerId (Silviu) Id=3333 DATA
# contractparcel ParcelId=1111 ContractId = zzz SellerId = 2222
#
# contract yyy BuyerId (Agro) Id=3334 DATA
# contractparcel ParcelId=1111 ContractId = yyy SellerId = 3333
# contractparcel ParcelId=1112 ContractId = yyy SellerId = 1112

#
# https://pypi.org/project/django-multiselectfield/
# https://medium.com/@yabiz/django-many-to-many-relationship-in-5-steps-a3af448e3702
# https://www.youtube.com/watch?v=6RGcWqcs8tw
# innerjoin
