from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic.detail import DetailView
from . import models
from django.urls import reverse_lazy


# Create your views here.
# view-uri pt Judet


class CountyCreateView(CreateView):
    model = models.County
    template_name = 'county_add.html'
    fields = '__all__'
    success_url = reverse_lazy('county_list')


class CountyListView(ListView):
    model = models.County
    template_name = 'county_list.html'
    context_object_name = 'all_county'


class CountyDetailView(DetailView):
    model = models.County
    template_name = 'county_details.html'
    context_object_name = 'county'


class CountyUpdateView(UpdateView):
    model = models.County
    template_name = 'county_update.html'
    context_object_name = 'county'
    fields = '__all__'
    success_url = reverse_lazy('county_list')


class CountyDeleteView(DeleteView):
    model = models.County
    template_name = 'county_update.html'
    context_object_name = 'county'
    success_url = reverse_lazy('county_list')

    # view-uri pt comuna


class VillageCreateView(CreateView):
    model = models.Village
    template_name = 'village_add.html'
    fields = '__all__'
    success_url = reverse_lazy('village_list')


class VillageListView(ListView):
    model = models.Village
    template_name = 'village_list.html'
    context_object_name = 'all_village'


class VillageDetailView(DetailView):
    model = models.Village
    template_name = 'village_details.html'
    context_object_name = 'village'


class VillageUpdateView(UpdateView):
    model = models.Village
    template_name = 'village_update.html'
    context_object_name = 'village'
    fields = '__all__'
    success_url = reverse_lazy('village_list')


class VillageDeleteView(DeleteView):
    model = models.Village
    template_name = 'village_delete.html'
    context_object_name = 'village'
    success_url = reverse_lazy('village_list')


# view-uri pt zona
class AreaCreateView(CreateView):
    model = models.Area
    template_name = 'area_add.html'
    fields = '__all__'
    success_url = reverse_lazy('area_list')


class AreaListView(ListView):
    model = models.Area
    template_name = 'area_list.html'
    context_object_name = 'all_area'


class AreaDetailView(DetailView):
    model = models.Area
    template_name = 'area_details.html'
    context_object_name = 'area'


class AreaUpdateView(UpdateView):
    model = models.Area
    template_name = 'area_update.html'
    context_object_name = 'area'
    fields = '__all__'
    success_url = reverse_lazy('area_list')


class AreaDeleteView(DeleteView):
    model = models.Area
    template_name = 'area_delete.html'
    context_object_name = 'area'
    success_url = reverse_lazy('area_list')


# view-uri pt proprietar

class OwnerCreateView(CreateView):
    model = models.Owner
    template_name = 'owner_add.html'
    fields = '__all__'
    success_url = reverse_lazy('owner_list')


class OwnerListView(ListView):
    model = models.Owner
    template_name = 'owner_list.html'
    context_object_name = 'all_owner'


class OwnerDetailView(DetailView):
    model = models.Owner
    template_name = 'owner_details.html'
    context_object_name = 'owner'


class OwnerUpdateView(UpdateView):
    model = models.Owner
    template_name = 'owner_update.html'
    context_object_name = 'owner'
    fields = '__all__'
    success_url = reverse_lazy('owner_list')


class OwnerDeleteView(DeleteView):
    model = models.Owner
    template_name = 'owner_delete.html'
    context_object_name = 'owner'
    success_url = reverse_lazy('owner_list')


# view-uri pt parcela
class ParcelCreateView(CreateView):
    model = models.Parcel
    template_name = 'parcel_add.html'
    fields = '__all__'
    success_url = reverse_lazy('parcel_list')


class ParcelListView(ListView):
    model = models.Parcel
    template_name = 'parcel_list.html'
    context_object_name = 'all_parcel'


class ParcelDetailView(DetailView):
    model = models.Parcel
    template_name = 'parcel_details.html'
    context_object_name = 'parcel'


class ParcelUpdateView(UpdateView):
    model = models.Parcel
    template_name = 'parcel_update.html'
    context_object_name = 'parcel'
    fields = '__all__'
    success_url = reverse_lazy('parcel_list')


class ParcelDeleteView(DeleteView):
    model = models.Parcel
    template_name = 'parcel_delete.html'
    context_object_name = 'parcel'
    success_url = reverse_lazy('parcel_list')


# view-uri pt contracte
class ContractCreateView(CreateView):
    model = models.Contract
    template_name = 'contract_add.html'
    fields = '__all__'
    success_url = reverse_lazy('contract_list')


class ContractListView(ListView):
    model = models.Contract
    template_name = 'contract_list.html'
    context_object_name = 'all_contract'


class ContractDetailView(DetailView):
    model = models.Contract
    template_name = 'contract_details.html'
    context_object_name = 'contract'


class ContractUpdateView(UpdateView):
    model = models.Contract
    template_name = 'contract_update.html'
    context_object_name = 'contract'
    fields = '__all__'
    success_url = reverse_lazy('contract_list')


class ContractDeleteView(DeleteView):
    model = models.Contract
    template_name = 'contract_delete.html'
    context_object_name = 'contract'
    success_url = reverse_lazy('contract_list')


class ContractParcelListView(ListView):
    model = models.ContractParcel
    template_name = 'contract_parcel_list.html'
    context_object_name = 'all_contract_parcel'


class ContractParcelDetailView(DetailView):
    model = models.ContractParcel
    template_name = 'contract_parcel_details.html'
    context_object_name = 'contract_parcel'


class ParcelAddView(ListView):
    model = models.Parcel
    template_name = 'contract_parcel_add.html'
    context_object_name = 'all_parcel'
    fields = '__all__'

# view-uri pt arenda
class LeaseCreateView(CreateView):
    model = models.Lease
    template_name = 'lease_add.html'
    fields = '__all__'
    success_url = reverse_lazy('lease_list')


class LeaseListView(ListView):
    model = models.Lease
    template_name = 'lease_list.html'
    context_object_name = 'all_lease'


class LeaseDetailView(DetailView):
    model = models.Lease
    template_name = 'lease_details.html'
    context_object_name = 'lease'


class LeaseUpdateView(UpdateView):
    model = models.Lease
    template_name = 'lease_update.html'
    context_object_name = 'lease'
    fields = '__all__'
    success_url = reverse_lazy('lease_list')


class LeaseDeleteView(DeleteView):
    model = models.Lease
    template_name = 'lease_delete.html'
    context_object_name = 'lease'
    success_url = reverse_lazy('lease_list')


class LeaseParcelListView(ListView):
    model = models.LeaseParcel
    template_name = 'lease_parcel_list.html'
    context_object_name = 'all_lease_parcel'


class LeaseParcelDetailView(DetailView):
    model = models.LeaseParcel
    template_name = 'lease_parcel_details.html'
    context_object_name = 'lease_parcel'


def contract_parcel_select(request):
    if request.method == "POST":
        print(request.POST)
    return HttpResponse("POST")
