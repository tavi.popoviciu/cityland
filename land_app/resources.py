from import_export import resources
from .models import *


class ParcelResource(resources.ModelResource):
    class Meta:
        model = Parcel



class ContractResource(resources.ModelResource):
    class Meta:
        model = Contract


class ContractParcelResource(resources.ModelResource):
    class Meta:
        model = ContractParcel


class LeaseResource(resources.ModelResource):
    class Meta:
        model = Lease


class LeaseParcelResource(resources.ModelResource):
    class Meta:
        model = LeaseParcel
