from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import *
@admin.register(Parcel)
class ParcelAdmin(ImportExportModelAdmin):
    pass

@admin.register(Contract)
class ContractAdmin(ImportExportModelAdmin):
    pass

@admin.register(Lease)
class LeaseAdmin(ImportExportModelAdmin):
    pass

